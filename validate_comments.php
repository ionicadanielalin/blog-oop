<?php
include "includes/functions.php";
include "includes/config.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog Dan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css"/>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>

    <script>
        function changeLang() {
            document.getElementById('form_lang').submit();
        }
    </script>

    <?php
    include "parts/header.php";
    include "parts/menu_admin.php";
    ?>
<div id=comments class="container">
    <h3 style="text-align: center">Validare comentarii</h3>
        <?php
        $comments = dbSelect('comments', ['status' => 'pending'], null, 0, null, null, null, null, 'AND');
        foreach ($comments as $comment): ?>
            <div class="row">
                <div class="col p-4 d-flex flex-column position-static">
                    User:<?php echo $comment['nickname'] ?>
                    <b><?php echo $comment['content'] ?></b>
                    <?php echo $comment['id']; ?>
                    -/<?php echo $comment['status']; ?>
                </div>

                <div class="btn-group-sm-horizontal">
                    <a href="validate_comments_header.php?id=<?php echo $comment['id']; ?>">
                        <button type="button" class="btn btn-outline-success">Valideaza</button>
                    </a>
                    <a href="delete_comments.php?id=<?php echo $comment['id']; ?>">
                        <button type="submit"
                                class="btn btn-outline-danger">Sterge....
                        </button>
                    </a>
                </div>
            </div>
            <hr>
        <?php endforeach; ?>
    </div>


    <?php include "parts/footer.php"; ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    </body>
</html>
