<?php
session_start();
?>

<div class="row">
<!-- Language -->
    <div class="col 4">
<form method='get' action='change_language.php' id='form_lang'>
    Alege o limba : <select name='lang' onchange='changeLang();'>
        <option value='ro' <?php if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'ro') {

            echo "selected";
        } ?> >Romana
        </option>
        <option value='en' <?php if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
            echo "selected";
        } ?> >English
        </option>
    </select>
</form>
    </div>
    <!-- Search buton -->
    <div class="col 4">
        <form action="search_last.php" method="get">
            <input type="text" name="search" placeholder="Searc for something..."/>
            <input type="submit"/>
        </form>
    </div>
    <!-- login -->
    <div class="col 4">

        <form action="login_cont.php" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="mail" placeholder="E-mail">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
            <button type="submit" class="btn btn-primary">Intra in cont</button>
        </form>
    </div>
</div>


    <div id="header">
        <!-- Logo -->
        <div class="row">
            <div class="logo">
                <a href="#"><img src="images/logo.png" alt="logo"></a>
            </div>
            <div class="textLogo">
                <?php
                if(check_lang() == true){
                   $lang = 'ro';
                } else {
                    $lang = 'en';
                }
                if ($lang == 'en') {
                    echo "<h1>Call of nature: RICH COUNTRY</h1>";
                } else {
                    echo "<h1>CHEMAREA NATURII: BOGATIILE TARII</h1>";

                }
                ?>
            </div>
        </div>
        <!-- MENU -->
<?php
if ($lang == 'en'){
    menu('en');
} else {
    menu('ro');
}
?>
</div>


