<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/20/2019
 * Time: 5:34 PM
 */
class Post extends BaseEntity
{
    public $title;

    public $content;

    public $author;

    public $language;

    public $category_id;

    public $author_id;

    public $showOnHomepage;


    public function getArticleImages()
    {
        $data = dbSelect('post_images',['post_id'=>$this->id]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new ArticleImage($productData['id']);
        }
        return $result;
    }

    public function getComments(){
        $data = dbSelect('comments',['product_id'=>$this->id]);
        $result = [];
        foreach ($data as $productData){
            $result[]=new Comment($productData['id']);
        }
        return $result;
    }

    public function getCategory(){
        return new Category($this->category_id);
    }

    public function articleSel ($filters)
    {
        $data = dbSelect('post', $filters);
        $result = [];
        foreach ($data as $productData) {
            $result[] = new Post($productData['id']);
        }
        return $result;
    }

    public function getPostComments()
    {
        $data = dbSelect('comment', ['post_id'=>$this->id , 'status'=>'visible'], []);

        $result = [];
        foreach ($data as $postData){
            $result[]=new Comment($postData['id']);
        }
        return $result;
    }


    public function getAuthor(){
        return new Authors($this->author_id);
    }


    public function getLatestArticle ()
    {
        $data = dbSelectOne('post', ['language' => 'ro'], [], 0, 1, 'date', 'DESC');

        return $data;
    }


    public function getPostsByDate()
    {
        $data = dbSelect('post', [], [], [], [], 'date', 'DESC');
        return $data;
    }

    public static function getHomepagePost(){
        $data = dbSelect('post',['showOnHomepage'=>1]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new Post($productData['id']);
        }
        return $result;
    }
    //in index il apelam in felul urmator:
   // $homepagePost=Post::getHomepagePost();
}
