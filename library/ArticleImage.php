<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/20/2019
 * Time: 5:35 PM
 */
class ArticleImage extends BaseEntity
{
    public $name;
    public $post_id;

    public function getTable()
    {
        return "post_images";
    }
}