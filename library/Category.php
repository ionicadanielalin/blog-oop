<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/20/2019
 * Time: 5:32 PM
 */
class Category extends BaseEntity
{

    public $name;

    public $parent_id;
    public $language;

    public function getArticles()
    {
        $data = dbSelect('post',['category_id'=>$this->id]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new Post($productData['id']);
        }

        return $result;
    }
}