<?php

/**
 * Created by PhpStorm.
 * User: Dan
 * Date: 8/21/2019
 * Time: 12:20 AM
 */
class Menu extends BaseEntity
{

    public $title;
    public $url;
    public $language;


    public function getTable()
    {
        return "menu";
    }
}
