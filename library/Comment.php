<?php



class Comment extends BaseEntity
{

    public $nickname;
    public $content;
    public $status='pending';
    public $post_id;

    public function getTable()
    {
        return "comments";
    }
}