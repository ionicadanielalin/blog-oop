<?php
include "config.php";

function dbInsert($table, $data)
{
    global $dbConnection;
    $columns = [];
    $values = [];
    $images = $_FILES['image'];

    foreach ($data as $column => $value) {
        $columns[] = "`" . mysqli_real_escape_string($dbConnection, $column) . "`";
        $values[] = "'" . mysqli_real_escape_string($dbConnection, $value) . "'";
    }
    $sqlcolumns = implode(',', $columns);
    $sqlvalues = implode(',', $values);
    $result = mysqli_query($dbConnection, "INSERT INTO $table($sqlcolumns) VALUES ($sqlvalues)");
    return mysqli_insert_id($dbConnection);
}

function dbDelete($table, $id)
{
    global $mysqlConnect;
    $result = mysqli_query($mysqlConnect, "DELETE FROM $table WHERE id=" . intval($id));

    return mysqli_affected_rows($mysqlConnect) > 0;
}

function dbUpdate($table, $id, $data)
{
    global $mysqlConnect;
    $sets = [];
    foreach ($data as $column => $value) {
        //$sets[]=" `$column`='$value'";
        //var_dum($sets);die;
        $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "`='" . mysqli_real_escape_string($mysqlConnect, $value) . "'";
    }
    $sqlSets = implode(',', $sets);
    $result = mysqli_query($mysqlConnect, "UPDATE $table SET $sqlSets  WHERE id=" . intval($id));
    return mysqli_affected_rows($mysqlConnect) > 0;

}

/**
 *  select all -
 *  select by -  WHERE user_id=5 AND active=1
 *  select like -
 *  limit
 *  offset
 *  sorting
 */
function dbSelect($table, $filters = null, $likeFilters = null, $offset = 0, $limit = null, $sortBy = null, $sortDirection = 'ASC', $groupBy = null, $operator = 'AND')
{
    global $mysqlConnect;
    $sql = "SELECT * FROM $table";

    if (($filters != null) || ($likeFilters != null)) {
        $sets = [];
        if ($filters != null) {
            foreach ($filters as $column => $value) {
                if ($value != null) {
                    $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "`='" . mysqli_real_escape_string($mysqlConnect, $value) . "'";
                }
            }
        }
        if ($likeFilters != null) {
            foreach ($likeFilters as $column => $value) {
                if ($value != null) {
                    $sets[] = "`" . mysqli_real_escape_string($mysqlConnect, $column) . "` LIKE '%" . mysqli_real_escape_string($mysqlConnect, $value) . "%'";


                }

            }
        }
        $sql .= ' WHERE ' . implode($operator, $sets);
    }

    if ($groupBy) {
        $sql .= ' GROUP BY ' . mysqli_real_escape_string($mysqlConnect, $groupBy);

    }
    if ($sortBy != null) {
        $sql .= ' ORDER BY ' . mysqli_real_escape_string($mysqlConnect, $sortBy) . ' ' . mysqli_real_escape_string($mysqlConnect, $sortDirection);
    }

    if ($limit != null) {
        $sql .= ' LIMIT ' . intval($offset) . ',' . intval($limit);
    }
    //die($sql);
    $result = mysqli_query($mysqlConnect, $sql);
    if (!$result) {
        die("SQL error: " . mysqli_error($mysqlConnect) . " SQL:" . $sql);
    }

    return $result->fetch_all(MYSQLI_ASSOC);

}


function dbSelectOne($table, $filters = [], $likeFilters = [], $offset = 0, $limit = null, $sortBy = null, $sortDirection = 'ASC')
{
    $data = dbSelect($table, $filters, $likeFilters, 0, 1, $sortBy, $sortDirection);
    return $data[0];
}

;


function showArticle($post)
{
    ?>
    <div>

        <h2><a href="articol.php?id=<?php echo $post->id; ?>"><?php echo $post->title; ?></a></h2>

        <a href="articol.php?id=<?php echo $post->id; ?>"><img align="center"
                                                               src="images/<?php echo $post->getArticleImages()[0]->name; ?>"
                                                               width="60%"></a>
        <p>
            <?php echo substr($post->content, 0, strpos($post->content, ' ', 255)); ?>...
        </p>
        <p>By: <i><?php echo $post->getAuthor()->name; ?></i></p>
        <br>
    </div>
    <?php
}

function showSimilarArticle($post)
{
    ?>
    <div class="col-4">
        <h2><a href="articol.php?id=<?php echo $post->id; ?>"><?php echo $post->title; ?></a></h2>

        <a href="articol.php?id=<?php echo $post->id; ?>"><img align="center"
                                                               src="images/<?php echo $post->getArticleImages()[0]->name; ?>"
                                                               width="60%"></a>
        <p>
            <?php echo substr($post->content, 0, strpos($post->content, ' ', 255)); ?>...
        </p>
        <p>By: <i><?php echo $post->getAuthor()->name; ?></i></p>
        <br>
    </div>
    <?php
}


function show_log_Article($article)
{
    ?>
    <div>

        <h2><a href="articol.php?key=<?php echo $article['id']; ?>"><?php echo $article['title']; ?></a></h2>
        <a href="articol.php?key=<?php echo $article['id']; ?>"><img align="center"
                                                                     src="images/<?php echo $article['image']; ?>"
                                                                     width="60%"></a>
        <p>
            <?php echo($article['content']); ?>
        </p>
        <p>By: <i><?php echo $article['Authors']; ?></i></p>
        <br>
        <div>
            <a class="btn btn-danger" href="admin_article.php?key=<?php echo $article['id']; ?>"
               role="button">Editeaza</a>
        </div>
    </div>
    <?php
}


function showAllArticle($article)
{
    ?>
    <div>
        <h2 style="text-align: center"><a
                    href="articol.php?key=<?php echo $article['id']; ?>"><?php echo $article['title']; ?></a></h2>
        <a href="articol.php?key=<?php echo $article['id']; ?>"><img align="center"
                                                                     src="images/<?php echo $article['image']; ?>"
                                                                     width="80%"></a>
        <p>
            <?php echo $article['content']; ?>
        </p>
        <p style="text-align: center">By: <i><?php echo $article['Authors']; ?></i></p>
        <br>
    </div>
    <?php
}


function menu($language)
{
    ?>
    <div id="menu">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">HOME</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <li class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <?php
                    $data = dbSelect('menu', ['language' => $language]);
                    foreach ($data as $menuItem) {
                        $item = new Menu($menuItem['id']);
                        //var_dump($item);
                        ?>
                        <li class="nav-item active"><a class="nav-link"
                                                       href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
                        </li>
                        <?php
                    }

                    ?>
                </ul>
            </li>
    </div>
    <?php
}


function check_lang()
{
    if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'ro') {
        return true;
    } else {
       return false;
    }



//on pages you want to check whether the language is romanian or english:
//if(check_login() == true){
// $lang='ro';
//} else {
// $lang='en';
//}


//function check_login()
//{
 //   if (isset($_SESSION['login']) && $_SESSION['login'] != '') {
  //      return true;
  //  } else {
  //     return false;
//    }
//}

//on pages you want to check whether the user is logged in or not, you can:
//if (check_login() === true) {
    //here you would put what you want to do if the user is logged in
//} else {
    //this would be executed if user isn't logged in
 //   header('Location: index.php');
 //   exit();
    //the above would redirect the user
}


//function checklogin()
//{
  //  return (isset($_SESSION['login'])) ? true : false;
//}


//on pages you want to check whether the user is logged in or not, you can:
//if (checklogin() === true) {
    //here you would put what you want to do if the user is logged in
//} else {
    //this would be executed if user isn't logged in
 //   header('Location: protected.php');
 //   exit();
    //the above would redirect the user
//}


