<?php
include "includes/functions.php";
include "includes/config.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog Dan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css"/>
</head>
<body>

<script>
    function changeLang() {
        document.getElementById('form_lang').submit();
    }
</script>

<?php
include "parts/header.php";
?>

<div id="container">
    <div class="row">
        <div class="contentIndex">
            <div>
                <?php
                global $mysqlConnect;
                $search_term = $_GET['search'];
                $articole = dbSelect('articles', [], ['title' => $search_term, 'content' => $search_term, 'Authors' => $search_term], 0, null, null, 'ASC', null, 'OR');
                if (empty($articole)) {
                    ?>
                    <h3> Nu au fost gasite rezultate. </h3>
                    <?php
                } else {
                    foreach ($articole as $key => $article) {
                        showAllArticle($article);
                    }
                }
                ?>
            </div>
        </div>

        <div id="sidebar">
            <?php include "parts/sidebar.php" ?>
        </div>
    </div>
</div>

<?php include "parts/footer.php"; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>

