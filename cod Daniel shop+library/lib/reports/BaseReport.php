<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/22/2019
 * Time: 7:28 PM
 */
abstract class BaseReport
{
    public $data;

    public function __construct()
    {
        global $dbConnection;
        //1 rulam query
        $result = mysqli_query($dbConnection, $this->getSql());
        //2 setam $data cu datele din query

        $this->data = $result->fetch_all(MYSQLI_ASSOC);

    }

    abstract function getSql();

    public function saveToCSV($filename){
        $fp = fopen($filename, 'w');
        fputcsv($fp, array_keys($this->data[0]));
        foreach ($this->data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }
}