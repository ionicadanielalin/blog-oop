<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/1/2019
 * Time: 7:34 PM
 */
class Category extends BaseEntity
{
    public $name;

    public $parent_id;

    public function getProducts()
    {
        $data = dbSelect('product',['category_id'=>$this->getId()]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new Product($productData['id']);
        }

        return $result;
    }
}