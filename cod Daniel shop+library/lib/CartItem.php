<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/22/2019
 * Time: 8:16 PM
 */
class CartItem extends BaseEntity
{
    public $product_id;

    public $quantity;

    public $cart_id;

    public function getTable()
    {
        return 'cart_item';
    }


    public function getProduct(){
        return new Product($this->product_id);
    }

    public function getCart(){
        return new Cart($this->cart_id);
    }

    public function getTotal(){
        if ($this->quantity>10){
            return $this->quantity * $this->getProduct()->getFinalPrice()*0.9;
        } else {
            return $this->quantity * $this->getProduct()->getFinalPrice();
        }
    }
}