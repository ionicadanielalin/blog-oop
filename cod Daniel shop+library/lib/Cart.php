<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/22/2019
 * Time: 8:11 PM
 */
class Cart extends BaseEntity
{
    const RAMBURS_PAYMENT = 'ramburs';

    public $user_id;

    public $address;

    public $phone;

    public $email;

    public $payment_method=self::RAMBURS_PAYMENT;

    public function getUser(){
        return new User($this->user_id);
    }

    /**
     * @return CartItem[]
     */
    public function getCartItems()
    {
        $data = dbSelect('cart_item',['cart_id'=>$this->getId()]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new CartItem($productData['id']);
        }

        return $result;
    }

    public function getTotal(){
        $total = 0;

        foreach ($this->getCartItems() as $cartItem){
            $total+=$cartItem->getTotal();
        }

        return $total;
    }

    public function add($product_id, $quantity){
        foreach ($this->getCartItems() as $cartItem) {
            if ($cartItem->product_id == $product_id) {
                $cartItem->quantity+= $quantity;
                if ($cartItem->quantity<=0){
                    $cartItem->delete();
                } else {
                    $cartItem->save();
                }
                return;
            }
        }
        $cartItem = new CartItem();
        $cartItem->cart_id = $this->getId();
        $cartItem->product_id = $product_id;
        $cartItem->quantity = $quantity;
        $cartItem->save();
    }

    public function update($product_id, $quantity){
        foreach ($this->getCartItems() as $cartItem){
            if ($cartItem->product_id==$product_id){
                $cartItem->quantity=$quantity;
                if ($quantity<=0){
                    $cartItem->delete();
                } else {
                    $cartItem->save();
                }
            }
        }
    }

    public function emptyCart(){
        foreach ($this->getCartItems() as $cartItem){
            $cartItem->delete();
        }
    }
}