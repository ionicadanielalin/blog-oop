<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/1/2019
 * Time: 6:58 PM
 */
class Product extends BaseEntity
{
    const STATUS_ACTIVE='active';

    const STATUS_INACTIVE='inactive';

    const STATUS_UNREAD='unread';

    public $name;

    public $description;

    public $price = 0;

    public $discount = 0;

    public $category_id;

    public $views;

    public $status = self::STATUS_UNREAD;


    public function getProductImages()
    {
        $data = dbSelect('product_image',['product_id'=>$this->getId()]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new ProductImage($productData['id']);
        }

        return $result;
    }

    public static function getHomepageProducts()
    {
        $data = dbSelect('product',['home_page'=>1]);

        $result = [];
        foreach ($data as $productData){
            $result[]=new Product($productData['id']);
        }

        return $result;
    }


    public function getFinalPrice(){
        return $this->price - $this->price*$this->discount;
    }

    public function getCategory(){
        return new Category($this->category_id);
    }

}