<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 8/12/2019
 * Time: 7:35 PM
 */
abstract class BaseEntity
{
    private $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    public function __construct($id=null)
    {
        if (!is_null($id)) {
            $data = dbSelectOne($this->getTable(), ['id' => $id]);
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
    }


    public function getTable()
    {
        return strtolower(static::class);
    }

    public function delete()
    {
        dbDelete($this->getTable(), $this->id);
    }

    public function save()
    {
        if (!is_null($this->id)) {
            dbUpdate($this->getTable(), $this->id, get_object_vars($this));
        } else {
            $this->id = dbInsert($this->getTable(), get_object_vars($this));
        }
    }

}