<?php
include "includes/functions.php";
session_start();

if (isset($_SESSION['cart_id'])){
    $cart = new Cart($_SESSION['cart_id']);
} else {
    $cart = new Cart();
    $cart->save();
    $_SESSION['cart_id'] = $cart->getId();
}


?>

<table border="1">
    <tr>
        <th>Product</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total</th>
    </tr>
    <?php foreach ($cart->getCartItems() as $cartItem): ?>
        <tr>
            <td><?php echo $cartItem->getProduct()->name; ?></td>
            <td>
                <?php echo $cartItem->quantity; ?>
                <a href="add_to_cart.php?product_id=<?php echo $cartItem->getProduct()->getId();?>&quantity=1">+</a>
                <a href="add_to_cart.php?product_id=<?php echo $cartItem->getProduct()->getId();?>&quantity=-1">-</a>
            </td>
            <td><?php echo $cartItem->getProduct()->getFinalPrice(); ?> RON</td>
            <td><?php echo $cartItem->getTotal(); ?></td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <th>Total</th>
        <th></th>
        <th></th>
        <th><?php echo $cart->getTotal(); ?></th>
    </tr>
</table>
