<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Blog Dan</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="main.css"/>
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-grid.css">
    <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<?php
include "includes/functions.php";
include "parts/header.php";
?>
<div id="container">
    <div class="row">
        <div class="articleCategory">
            <?php
            global $mysqlConnect;

            if (isset($_GET['category'])) {
               // dbSelect($table, $filters = null, $likeFilters = null, $offset = 0, $limit = null, $sortBy = null, $sortDirection = 'ASC', $groupBy = null, $operator = 'AND')
                $articles = dbSelect('articles',null, null, 0, null, null, null, null, "AND");
                $results = dbSelect('articles', ['category' => $_GET['category']], null, 0, null, null, null, null, 'AND');
                //$result = mysqli_query($mysqlConnect, "SELECT * FROM articles WHERE category='" . $_GET['category'] . "'");
            //var_dump($results);die;
            } else {
                $results = dbSelect('articles', ['Authors' => $_GET['Authors']], null, 0, null, null, null, null, 'AND');
                //$result = mysqli_query($mysqlConnect, "SELECT * FROM articles WHERE author='" . $_GET['author'] . "'");
              //  var_dump($results);die;
            }
            //$articles = $result->fetch_all(MYSQLI_ASSOC);
            foreach ($results as $key => $result) {
                if (isset($_SESSION['lang']) && $_SESSION['lang'] == 'en') {
                    $result['titlex'] = $result['title_en'];
                    $result['contentx'] = $result['content_en'];
                    showArticle($result);
                } else {
                    $result['titlex'] = $result['title'];
                    $result['contentx'] = $result['content'];
                    showArticle($result);
                }
            }
            ?>
        </div>
        <div id="sidebar">
            <?php include "parts/sidebar.php" ?>
        </div>
    </div>
</div>


<?php include "parts/footer.php"; ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>